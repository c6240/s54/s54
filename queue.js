
let collection = [];

// Write the queue functions below.


function print() {
    //Print queue elements.
    return collection
}
print()

function enqueue(names){
    collection.push(names)
    
    return collection
}
// enqueue('John')
// enqueue('Jane')

function dequeue(){
    collection.shift()
    return collection
}

dequeue()
// enqueue('Bob')
// enqueue('Cherry')

function front(){
    return collection[0]
}
front()

function size(){
    return collection.length
}
size()


function isEmpty(){
    if(collection == 0){
        return true
    } else{
        return false
    }
}

isEmpty()



module.exports = {
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};